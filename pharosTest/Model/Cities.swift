//
//  Cities.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct Cities : Decodable{
    var country = String()
    var name = String()
    var id = String()
    var coord = CoordData()
    
    enum CodingKeys: String, CodingKey {
        case country = "country"
        case name = "name"
        case id = "_id"
        case coord = "coord"
    }
}

struct CoordData : Decodable {
    var lon : String?
    var lat : String?
}

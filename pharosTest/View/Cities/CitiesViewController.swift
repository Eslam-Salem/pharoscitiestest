//
//  ViewController.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {
    
    //MARK: - OUTLETS AND VARIABLES
    @IBOutlet weak var citiesTableView : UITableView!
    @IBOutlet weak var searchBar : UISearchBar!
    
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    var viewModel = CitiesViewModel()
    var pageNum = 1
    //MARK: - Screen LifyCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.initViewModel()
    }
    //MARK: - HELPER Methods
    func initView (){
        self.title = "Cities"
        citiesTableView.delegate = self
        citiesTableView.dataSource = self
        searchBar.delegate = self
        activityIndicator.center = view.center
        addToolBar(searchBar: self.searchBar)
    }
    
    func initViewModel (){
        self.viewModel.initFetch(pageNumber: "\(self.pageNum)")
        self.viewModel.reloadTableViewClosure = {
            DispatchQueue.main.async {
                self.citiesTableView.reloadData()
            }
        }
        self.viewModel.startActivityIndicatorClosure = {
            self.activityIndicator(isStart: true)
        }
        self.viewModel.stopActivityIndicatorClosure = {
            self.activityIndicator(isStart: false)
        }
        self.viewModel.showAlertMessageClosure = {
            self.showAlert(self.viewModel.alertMessage)
        }
    }
    //show alert message
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    // activity indicator in two cases start activity or ending it
    func activityIndicator (isStart : Bool){
        if isStart{
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }else{
            activityIndicator.removeFromSuperview()
            activityIndicator.stopAnimating()
        }
    }
}

//MARK: - EXTENSION FOR TABLE VIEW DELEGATE AND DATA SOURCE
extension CitiesViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print ("====>>>>" , self.viewModel.numberOfCells)
        return self.viewModel.numberOfCells
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CitiesTableViewCell") as! CitiesTableViewCell
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.CityCell = cellVM // configure cell data
        print(indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Map", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "MapViewController") as MapViewController
        vc.modalPresentationStyle = .popover
        viewModel.sendCoordToMapViewModel(at: indexPath)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard self.searchBar.text == "" else {return}
        
        if indexPath.row == (self.viewModel.numberOfCells ) - 1  {
            print("loading")
            print(indexPath.row)
            self.pageNum += 1
            self.viewModel.initFetch(pageNumber: "\(self.pageNum)")
        }
    }
    
}
//MARK: - EXTENSION FOR UISearchBarDelegate
extension CitiesViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text ?? "")
        self.viewModel.filterBy(searchText: searchBar.text ?? "")
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

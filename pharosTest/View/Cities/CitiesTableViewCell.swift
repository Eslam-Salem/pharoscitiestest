//
//  CitiesTableViewCell.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Kingfisher

class CitiesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLbl : UILabel!
    @IBOutlet weak var countryNameLbl : UILabel!
    @IBOutlet weak var cityStaticImageView : UIImageView!
    
    var CityCell : CityCellViewModel? {
        didSet {
            cityNameLbl.text =  "City name: \(CityCell?.cityName ?? "")"
            countryNameLbl.text = "Country name: \(CityCell?.countryName ?? "")"
            let imageUrl = URL(string: CityCell?.cityStaticImage ?? "")
            cityStaticImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "54361720_1923844597725646_599066900349059072_n"))
        }
    }
}

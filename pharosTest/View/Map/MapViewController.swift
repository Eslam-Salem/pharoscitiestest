//
//  MapViewController.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    //MARK: - OUTLETS
    @IBOutlet weak var wholeView : UIView!
    @IBOutlet weak var mapView : UIView!
    
    //MARK: - Screen LifyCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cities"
        initMap()
    }
    //MARK: - Helper Methods
    func initMap() {
        guard MapViewModel.cityLon != nil && MapViewModel.cityLat != nil else {return}
        let camera = GMSCameraPosition.camera(withLatitude: Double(MapViewModel.cityLat)!, longitude: Double(MapViewModel.cityLon)!, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = mapView
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(MapViewModel.cityLat)!, longitude: Double(MapViewModel.cityLon)!)
        marker.map = mapView
    }
}

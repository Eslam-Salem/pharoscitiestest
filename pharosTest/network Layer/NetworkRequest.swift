//
//  NetworkRequest.swift
//  testInova
//
//  Created by Mac on 1/15/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire

class NetworkRequest{
    func Request(url : String ,method: HTTPMethod, parameters: [String : Any]?, headers: [String: String]?, completionHundler: @escaping (DataResponse<Any>?,ErrorHandler?)->Void){
        print (url)
        Alamofire.request(url, method: method, parameters: parameters ?? [String: Any](),encoding: URLEncoding.default, headers: APIs.Instance.getHeader()).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)

                        print(err.message ?? "errorerrorerror")
                        completionHundler(nil, err)
                    }catch{
                        print("errorrrrelse")
                        print(error)
                    }
                }else{
                    print(response.data!)
                    completionHundler(response,nil)
                }
            case .failure(_):
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                print(lockString)
                completionHundler(nil,nil)
                break
            }
        }
    }
}

//
//  APIService.swift
//  MVVMPlayground
//
//  Created by Neo on 01/10/2017.
//  Copyright © 2017 ST.Huang. All rights reserved.
//

import Foundation

protocol APIServiceProtocol {
    func getCities(pageNumber: String , completionHundler: @escaping ([Cities]?,String?) -> Void)
}

class APIService: APIServiceProtocol {
    // Simulate a long waiting for fetching
     func getCities(pageNumber: String , completionHundler: @escaping ([Cities]?,String?) -> Void){
        NetworkRequest().Request(url: APIs.Instance.getCities(pageNum : pageNumber), method: .get, parameters: nil, headers: nil){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                           let cities = try JSONDecoder().decode([Cities].self,from:(response?.data)!)
                           completionHundler(cities,nil)
                       }catch{
                        print(error)
                        completionHundler(nil, error.localizedDescription)
                       }
                   }else{
                       completionHundler(nil, error?.message)
                   }
               }
           }
       }
}








//
//  ViewController.swift
//  Rezerva
//
//  Created by Mohamed Nawar on 5/4/19.
//


import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}

    private let url = "http://assignment.pharos-solutions.de/"

    public func getHeader() -> [String: String]{
        let header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \("")"  , "Content-Type":"application/x-www-form-urlencoded"
        ]
        return header
    }
    public func getCities(pageNum : String) -> String{
        return url + "cities.json?page=\(pageNum)"
    }

}

//
//  CityCellViewModel.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct CityCellViewModel : Equatable{
    var cityName : String?
    var countryName : String?
    var cityStaticImage : String?
    var cityLon : String?
    var cityLat : String?
}

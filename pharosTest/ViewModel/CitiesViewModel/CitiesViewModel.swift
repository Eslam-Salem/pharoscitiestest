//
//  CitiesViewModel.swift
//  pharosTest
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

class CitiesViewModel {
    //MARK: - VARIABLES
    let apiService: APIServiceProtocol  // network protocol
    var cellDataWarehouse = [CityCellViewModel]() // contain parsed data as city cell data to be warehouse for cell data (filtered or not)
    var cellViewModels = [CityCellViewModel]() { // var observer from cell View model to reload table view
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    var numberOfCells: Int {
          return cellViewModels.count
    }
    var alertMessage = String(){
        didSet{
            self.showAlertMessageClosure?()
        }
    }
    var isLastPage = false //Flag to check end of pages
    // CLOUSERS
    var reloadTableViewClosure: (() ->())?
    var startActivityIndicatorClosure: (() ->())?
    var stopActivityIndicatorClosure: (() ->())?
    var showAlertMessageClosure: (() ->())?
    
    //MARK: - HELPER FUNCTIONS
    init( apiService: APIServiceProtocol = APIService()) {
        self.apiService = apiService
    }
    // CALL API AND HANDLE ITS RESPONSE
    func initFetch(pageNumber: String) {
        if !isLastPage { // check if data isnot ended and request api
            startActivityIndicatorClosure?()
            apiService.getCities(pageNumber: pageNumber, completionHundler: handleResponse(cities:error:))
        }
    }
    func handleResponse(cities: [Cities]?, error: String?) {
        if let error = error{
            stopActivityIndicatorClosure?()
            self.alertMessage = error
        }else if error == nil && cities == nil {
            stopActivityIndicatorClosure?()
            self.alertMessage = "Something went wrong please try again later.."
        }else{
            stopActivityIndicatorClosure?()
            guard var parsedCities = cities else {return}
            // change flag to true if cities array count return with zero
            if parsedCities.count == 0 {
                self.isLastPage = true
            }
            // SORT CITIES
            parsedCities.sort(by: sortCriteria)
            //LOOPING ON EACH ELEMENT IN PARSED ARRAY TO append on cellDataWarehouse.
            for city in parsedCities{
                cellDataWarehouse.append(createCellViewModel(city: city))
            }
            self.cellViewModels = cellDataWarehouse //change cellViewModels to reload table view
        }
    }
    // RETURN THE DATA NEEDED FOR THE CELL FROM PARSED ARRAY ELEMENT (CityCellViewModel)
    func createCellViewModel(city: Cities) -> CityCellViewModel {
        let cityName = city.name
        let cityLat = city.coord.lat
        let cityLon = city.coord.lon
        let countryName = city.country 
        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=\(cityLat ?? ""),\(cityLon ?? "")&\("zoom=15&size=\(100)x\(100)")&sensor=true"
        
        return CityCellViewModel(cityName: cityName, countryName: countryName ,cityStaticImage: staticMapUrl, cityLon: cityLon, cityLat: cityLat)
    }

    //GET DATA OF CELL WITH SPECIFIC INDEXPATH
    func getCellViewModel( at indexPath: IndexPath ) -> CityCellViewModel {
        return cellViewModels[indexPath.row]
    }
    //SEND COORDINATE WITH SPECIFIC CELL TO MAP VC
    func sendCoordToMapViewModel (at indexPath: IndexPath ){
        MapViewModel.cityLat = cellViewModels[indexPath.row].cityLat
        MapViewModel.cityLon = cellViewModels[indexPath.row].cityLon
    }
    //SORTING CRITERIA (city first, country after)
    func sortCriteria(this:Cities, that:Cities) -> Bool {
        if this.name == that.name {
            return this.country < that.country
        }
      return this.name < that.name
    }
    // filtering method
    func filterBy (searchText : String){
        if searchText != ""{
            self.cellViewModels = self.cellDataWarehouse.filter({$0.cityName?.prefix(searchText.count) == searchText.prefix(searchText.count)})
        }else{
            self.cellViewModels = self.cellDataWarehouse
        }
    }
    
}

//
//  pharosTestTests.swift
//  pharosTestTests
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import XCTest
@testable import pharosTest

class pharosTestTests: XCTestCase {
    
    let alexandria = CityCellViewModel(cityName: "alexandria", countryName: "" ,cityStaticImage: "", cityLon: "", cityLat: "")
    let aswan = CityCellViewModel(cityName: "aswan", countryName: "" ,cityStaticImage: "", cityLon: "", cityLat: "")
    let cairo = CityCellViewModel(cityName: "cairo", countryName: "" ,cityStaticImage: "", cityLon: "", cityLat: "")
    
    var warehouse = [CityCellViewModel]()
    
    let sut = CitiesViewModel()

    override func setUp() {

        super.setUp()
        warehouse = [alexandria,aswan,cairo]
        sut.cellDataWarehouse = warehouse
        
    }

    override func tearDown() {

        sut.cellDataWarehouse = []
        warehouse = []
        super.tearDown()
    }

    func testFilter(){
        sut.filterBy(searchText: "a")
        XCTAssertTrue(sut.cellViewModels == [alexandria , aswan])
        sut.filterBy(searchText: "al")
        XCTAssertTrue(sut.cellViewModels == [alexandria])
        sut.filterBy(searchText: "")
        XCTAssertTrue(sut.cellViewModels == warehouse)
        sut.filterBy(searchText: "as")
        XCTAssertTrue(sut.cellViewModels == [aswan])
        sut.filterBy(searchText: "zx")
        XCTAssertTrue(sut.cellViewModels == [])
        sut.filterBy(searchText: "c")
        XCTAssertTrue(sut.cellViewModels == [cairo])
        sut.filterBy(searchText: "**")
        XCTAssertTrue(sut.cellViewModels == [])
    }

}
